#ifndef LIGHT_CPP
#define LIGHT_CPP
#include <Arduino.h>

class Light {
private:
  int enable;
  int pin;
  int id;

public:
  Light(const int pin, const int id)
  {
    this->pin = pin;
    this->id = id;
    pinMode(this->pin, OUTPUT);
    this->off();
  }

  int getId() {
    return this->id;
  }

  void off() {
    if(this->enable == HIGH) {
      return;
    }
    this->enable = HIGH;
    digitalWrite(this->pin, this->enable);
  }

  void on() {
    if(this->enable == LOW) {
      return;
    }
    this->enable = LOW;
    digitalWrite(this->pin, this->enable);
  }

  bool isOff() {
      return this->enable == HIGH;
  }

  bool isOn() {
      return this->enable == LOW;
  }

  void toggle() {
    this->isOn() ? this->off() : this->on();
  }
};

#endif LIGHT_CPP