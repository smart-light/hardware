#include <Arduino.h>

class WiFiCredentials {
private:
  char* ssid;
  char* pass;
public:
  WiFiCredentials(char* _ssid, char* _pass) {
    this->ssid = _ssid;
    this->pass = _pass;
  }

  char* getSsid() {
    return this->ssid;
  }

  const char* getPass() {
    return this->pass;
  }
};