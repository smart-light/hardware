#include <Arduino.h>
#include <light.cpp>
#include <wifi_credentials.cpp>
#include <ESP8266WiFi.h>
#include <AsyncMqttClient.h>

const int lightId = 1;
const int lightSecondaryId = 2;

WiFiCredentials *wifiCredentials = new WiFiCredentials("smartlight", "smartlight12345");
const uint8_t light_len = 2;
Light *lights[light_len];
AsyncMqttClient mqttClient;
IPAddress mqttIp = IPAddress(161, 35, 72, 169);
#pragma region 
const char* mqttUsername = "singlesly";
const char* mqttPassword = "Starix314789520";
const int mqttPort = 1883;
#pragma endregion

void onWiFiConnect(const WiFiEventStationModeGotIP &event);
void onWiFiDisconnect(const WiFiEventStationModeDisconnected &event);
void onMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total);
void onMqttDisconnect(AsyncMqttClientDisconnectReason reason);
void onMqttConnect(bool sessionPresent);
void lightReport();
void wifiConnect();
void mqttConnect();

void setup() {
  Serial.begin(74880);

  lights[0] = new Light(4, lightId);
  lights[1] = new Light(12, lightSecondaryId);

  mqttClient.setServer(mqttIp, mqttPort);
  mqttClient.setCredentials(mqttUsername, mqttPassword);
  mqttClient.onMessage(onMessage);
  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);

  WiFi.onStationModeGotIP(onWiFiConnect);
  WiFi.onStationModeDisconnected(onWiFiDisconnect);
  wifiConnect();
  mqttConnect();
 
}

void wifiConnect() {
  WiFi.disconnect();
  WiFi.begin(wifiCredentials->getSsid(), wifiCredentials->getPass());
  while(!WiFi.isConnected()) {
    delay(500);
    Serial.print(".");
  }
}

void mqttConnect() {
  if(mqttClient.connected()) {
    return;
  }
  
  char clientId[10];
  sprintf(clientId, "light-%d", lightId + lightSecondaryId);
  mqttClient.setClientId(clientId);
  mqttClient.connect();
  while(!mqttClient.connected()) {
     if(!WiFi.isConnected()) {
      return;
    }
    Serial.print("!");
    delay(500);
  }
}

void onMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  for(int i = 0; i < light_len; i++) {
    char expectedTopic[64];
    sprintf(expectedTopic, "v1/light/%d", int(lights[i]->getId()));
    if(strcmp(expectedTopic, topic) == 0) {
      atoi(payload) == 0 ? lights[i]->off() : lights[i]->on();
    }
  }
}

void onMqttConnect(bool sessionPresent) {
  if(mqttClient.connected()) {
    for(int i = 0; i < light_len; i++) {
      char topic[64];
      sprintf(topic, "v1/light/%d", int(lights[i]->getId()));
      mqttClient.subscribe(topic, 0);
    }
    Serial.println("\n Subscribed");
  }
}

void onWiFiDisconnect(const WiFiEventStationModeDisconnected &event) {
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  for(int i = 0; i < light_len; i++) {
    char topic[64];
    sprintf(topic, "v1/light/%d", int(lights[i]->getId()));
    mqttClient.unsubscribe(topic);
  }
  Serial.println((int)reason);

}

void onWiFiConnect(const WiFiEventStationModeGotIP &event) {
  Serial.println("Connected to wifi");
}

unsigned long last = 0;

void lightReport() {
  for(int i = 0; i < light_len; i++) {
    char payload[2];
    sprintf(payload, "%d", int(lights[i]->isOn()));
    char topic[64];
    sprintf(topic, "v1/light/%d/report", int(lights[i]->getId()));
    mqttClient.publish(topic, 0, false, payload);
  }
}

void loop() {
  
  if(!WiFi.isConnected()) {
    wifiConnect();
  }
  
  if(WiFi.isConnected() && !mqttClient.connected()) {
    mqttConnect();
  }

  if(WiFi.isConnected() && mqttClient.connected()) {
    if(millis() > (last + 5000)) {
      lightReport();
      last = millis();
    }
  }
}
